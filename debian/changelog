qmapshack (1.17.1-2) UNRELEASED; urgency=medium

  * Replace pkg-config build dependency with pkgconf.
  * Bump Standards-Version to 4.7.0, no changes.

 -- Bas Couwenberg <sebastic@debian.org>  Tue, 06 Feb 2024 09:37:53 +0100

qmapshack (1.17.1-1) unstable; urgency=medium

  * New upstream release.
  * Enable Salsa CI.
  * Don't compress .qch & .qhc files.
    (closes: #1053639)
  * Update copyright file.
  * Drop spelling-errors.patch, applied upstream. Refresh remaining patches.

 -- Bas Couwenberg <sebastic@debian.org>  Tue, 12 Dec 2023 14:34:55 +0100

qmapshack (1.17.0-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.6.2, no changes.
  * Bump debhelper compat to 13.
  * Update lintian overrides.
  * Update copyright file.
  * Refresh patches.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 21 Jul 2023 06:33:51 +0200

qmapshack (1.16.1-2) unstable; urgency=medium

  * Bump Standards-Version to 4.6.1, no changes.
  * Add Rules-Requires-Root to control file.
  * Update lintian overrides.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 01 Dec 2022 18:12:06 +0100

qmapshack (1.16.1-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.6.0, no changes.
  * Bump debhelper compat to 12, changes:
    - Drop --list-missing from dh_install
  * Add pkg-config to build dependencies.
  * Refresh patches.
  * Update lintian overrides.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 04 Dec 2021 12:44:01 +0100

qmapshack (1.16.0-2) unstable; urgency=medium

  * Update proj.patch with proj.h support.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 03 Sep 2021 22:28:03 +0200

qmapshack (1.16.0-1) unstable; urgency=medium

  * Move from experimental to unstable.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 15 Aug 2021 13:35:10 +0200

qmapshack (1.16.0-1~exp1) experimental; urgency=medium

  * New upstream release.
    (closes: #983260)
  * Update upstream metadata.
  * Update watch file for GitHub URL changes.
  * Update copyright file.
  * Refresh patches.
  * Add patch to fix spelling errors.

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 24 May 2021 13:08:59 +0200

qmapshack (1.15.2-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.5.1, no changes.

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 07 Dec 2020 17:35:29 +0100

qmapshack (1.15.1-1) unstable; urgency=medium

  * New upstream release.
  * Drop patches applied/included upstream.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 27 Nov 2020 08:51:18 +0100

qmapshack (1.15.0-2) unstable; urgency=medium

  * Bump watch file version to 4.
  * Add upstream patch to fix FTBFS with Qt 5.15.
  * Add upstream patch to fix wrong timestamp when loading FIT.
  * Update lintian overrides.

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 16 Nov 2020 11:34:35 +0100

qmapshack (1.15.0-1) unstable; urgency=medium

  * Move from experimental to unstable.

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 15 Jun 2020 19:06:16 +0200

qmapshack (1.15.0-1~exp1) experimental; urgency=medium

  * New upstream release.
    (closes: #962622)
  * Drop spelling-errors.patch, applied upstream.
  * Add patch to fix new spelling errors.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 14 Jun 2020 15:57:20 +0200

qmapshack (1.14.1-1) unstable; urgency=medium

  * New upstream release.
  * Drop Name field from upstream metadata.
  * Reorder dependencies.
  * Bump Standards-Version to 4.5.0, no changes.
  * Bump debhelper compat to 10, changes:
    - Drop --parallel option, enabled by default
  * Update copyright years for Henri Hornburg.
  * Add patch to fix spelling errors.
  * Add lintian override for arch-dep-package-has-big-usr-share.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 27 Mar 2020 12:21:41 +0100

qmapshack (1.14.0-1) unstable; urgency=medium

  * New upstream release.
  * Update URLs for move to GitHub.
  * Bump Standards-Version to 4.4.1, no changes.
  * Add Johannes Zellner to copyright holders.
  * Drop spelling-errors.patch, applied upstream.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 01 Nov 2019 09:29:57 +0100

qmapshack (1.13.2-1) unstable; urgency=medium

  * New upstream release.
  * Add patch to fix spelling errors.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 11 Sep 2019 18:31:46 +0200

qmapshack (1.13.1-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.4.0, no changes.

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 22 Jul 2019 16:09:42 +0200

qmapshack (1.13.0-1) unstable; urgency=medium

  * Update gbp.conf to use --source-only-changes by default.
  * Move from experimental to unstable.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 07 Jul 2019 10:27:33 +0200

qmapshack (1.13.0-1~exp1) experimental; urgency=medium

  * New upstream release.
  * Remove package name from lintian overrides.
  * Update copyright file, changes:
    - Add Henri Hornburg to copyright holders
    - Add license & copyright for geocaching icons
  * Add patches for missing Find*.cmake files.

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 08 Apr 2019 19:05:25 +0200

qmapshack (1.12.3-1) unstable; urgency=medium

  * New upstream release.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 08 Feb 2019 17:22:14 +0100

qmapshack (1.12.2-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.3.0, no changes.

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 04 Feb 2019 19:42:41 +0100

qmapshack (1.12.1-1) unstable; urgency=medium

  * New upstream release.
  * Update copyright years for Michel Durand.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 21 Dec 2018 15:18:29 +0100

qmapshack (1.12.0-1) unstable; urgency=medium

  * New upstream release.
  * Drop .gitignore file.
  * Bump Standards-Version to 4.2.1, no changes.
  * Update build dependencies for Qt5WebEngineWidgets & Qt5Qml.
  * Drop patches, included upstream.

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 03 Sep 2018 21:36:36 +0200

qmapshack (1.11.1-4) unstable; urgency=medium

  * Drop autopkgtest to test installability.
  * Add lintian override for testsuite-autopkgtest-missing.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 01 Aug 2018 18:34:27 +0200

qmapshack (1.11.1-3) unstable; urgency=medium

  * Add upstream patches to fix Qt 5.11 related issues.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 19 Jul 2018 09:39:21 +0200

qmapshack (1.11.1-2) unstable; urgency=medium

  * Strip trailing whitespace from control & rules files.
  * Bump Standards-Version to 4.1.5, no changes.
  * Don't bother to regenerate the icons.
    (LP: 1780046)

 -- Bas Couwenberg <sebastic@debian.org>  Tue, 17 Jul 2018 18:55:27 +0200

qmapshack (1.11.1-1) unstable; urgency=medium

  * New upstream release.
  * Update Vcs-* URLs for Salsa.
  * Bump Standards-Version to 4.1.4, no changes.
  * Drop patches, applied upstream.
  * Add lintian overrides for spelling-error-in-binary false positives.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 19 Apr 2018 15:11:19 +0200

qmapshack (1.11.0-1) unstable; urgency=medium

  * Move from experimental to unstable.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 09 Mar 2018 10:25:36 +0100

qmapshack (1.11.0-1~exp1) experimental; urgency=medium

  * New upstream release.
  * Update copyright-format URL to use HTTPS.
  * Bump Standards-Version to 4.1.3, no changes.
  * Update copyright years for Oliver Eichler & Norbert Truchsess.
  * Update compass image paths.
  * Add patch to fix build failure with -Werror=format-security.
  * Add patch for qmt_map2jnx manpage.
  * Add lintian override for hardening-no-fortify-functions.

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 05 Mar 2018 15:32:55 +0100

qmapshack (1.10.0-1) unstable; urgency=medium

  * Move from experimental to unstable.

 -- Bas Couwenberg <sebastic@debian.org>  Tue, 26 Dec 2017 22:42:01 +0100

qmapshack (1.10.0-1~exp1) experimental; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.1.2, no changes.
  * Add lintian override for spelling-error-in-binary false positive.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 21 Dec 2017 17:00:39 +0100

qmapshack (1.9.1-1) unstable; urgency=medium

  * New upstream release.
  * Update copyright years for Christian Eichler.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 17 Sep 2017 19:32:06 +0200

qmapshack (1.9.0-1) unstable; urgency=medium

  * New upstream release.
  * Update gbp.conf to use common configuration.

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 24 Jul 2017 18:11:29 +0200

qmapshack (1.8.1-1) unstable; urgency=medium

  * Bump Standards-Version to 4.0.0, no changes.
  * Add autopkgtest to test installability.
  * Move from experimental to unstable.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 18 Jun 2017 19:01:16 +0200

qmapshack (1.8.1-1~exp1) experimental; urgency=medium

  * New upstream release.
  * Drop patches, applied upstream.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 14 May 2017 13:24:24 +0200

qmapshack (1.8.0-1~exp1) experimental; urgency=medium

  * New upstream release.
  * Update copyright file, changes:
    - Update copyright years for Oliver Eichler
    - Add Michel Durand & Norbert Truchsess to copyright holders
  * Drop patches, applied upstream.
  * Add libquazip5-dev to build dependencies.
  * Add patch to fix quazip.h search paths.
  * Add patch to fix Russian desktop file translation.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 26 Mar 2017 13:58:02 +0200

qmapshack (1.7.2-1) unstable; urgency=medium

  * New upstream release.
  * Disable icon regeneration on mips64el, inkscape fails there too.
  * Add libalglib-dev to build dependencies.
  * Add license & copyright for alglib.
  * Add patch to tweak FindALGLIB.cmake for Debian package.
  * Add patch to fix spelling errors.

 -- Bas Couwenberg <sebastic@debian.org>  Tue, 13 Dec 2016 17:13:01 +0100

qmapshack (1.7.1-1) unstable; urgency=medium

  * New upstream release.

 -- Bas Couwenberg <sebastic@debian.org>  Tue, 13 Sep 2016 14:19:02 +0200

qmapshack (1.7.0-1) unstable; urgency=medium

  * New upstream release.
  * Disable icon regeneration on hurd-i386, inkscape fails there too.
  * Update copyright file, add Rainer Woitok to copyright holders.
  * Drop original-typo.patch, applied upstream.

 -- Bas Couwenberg <sebastic@debian.org>  Tue, 13 Sep 2016 10:06:55 +0200

qmapshack (1.6.3-1) unstable; urgency=medium

  * New upstream release.
  * Drop separate Files section for CTextEditWidget.{cpp,h}, now GPL-3+ too.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 14 Jul 2016 13:33:10 +0200

qmapshack (1.6.2-1) unstable; urgency=medium

  * New upstream release.
  * Update watch file to handle new tag convention and filenamemangling.
  * Drop patches applied upstream, refresh remaining patch.
  * Drop custom hicolor icons installation, fixed upstream.
  * Update copyright file, update years for Ivo Kronenberg,

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 03 Jul 2016 16:46:33 +0200

qmapshack (1.6.1-3) unstable; urgency=medium

  * Add patch to fix build failure with Qt5 < 5.4.
  * Install application icons in hicolor theme.
  * Bump Standards-Version to 3.9.8, changes: hicolor icons.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 05 May 2016 16:36:19 +0200

qmapshack (1.6.1-2) unstable; urgency=medium

  * Add patch for kFreeBSD support.
  * Add patch for Hurd support.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 26 Mar 2016 12:22:10 +0100

qmapshack (1.6.1-1) unstable; urgency=medium

  * New upstream release.
  * Enable all hardening buildflags.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 24 Mar 2016 21:15:01 +0100

qmapshack (1.6.0-2) unstable; urgency=medium

  * Require at least libroutino-dev 3.1 for Routino_Version symbol.
  * Drop routino-version.patch, not required for Routino 3.1.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 06 Mar 2016 18:32:07 +0100

qmapshack (1.6.0-1) unstable; urgency=medium

  * New upstream release.
  * Update Vcs-Git URL to use HTTPS.
  * Bump Standards-Version to 3.9.7, no changes.
  * Add patches for various typos.
  * Update copyright files, changes:
    - Update copyright years for Oliver Eichler & Christian Eichler
    - Add Peter Schumann & Ivo Kronenberg to copyright holders
  * Drop debugging-typo.patch, applied upstream.
    Refresh original-typo.patch, partially applied upstream.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 25 Feb 2016 22:15:38 +0100

qmapshack (1.5.1-1) unstable; urgency=medium

  * New upstream release.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 22 Nov 2015 18:49:33 +0100

qmapshack (1.5.0-1) unstable; urgency=medium

  * New upstream release.
  * Update paths for moved files in copyright file.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 22 Nov 2015 17:00:59 +0100

qmapshack (1.4.0-1) unstable; urgency=medium

  * New upstream release.
  * Update copyright file, changes:
    - Group copyright holders by license
    - Add Christian Eichler to copyright holders
  * Drop hicolor-icons.patch, applied upstream.
  * Add patch to drop Routino_Version requirement,
    not available in Routino 3.0 yet.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 07 Nov 2015 15:00:41 +0100

qmapshack (1.3.1-3) unstable; urgency=medium

  * Disable icon regeneration on powerpc, inkscape fails there too.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 23 Oct 2015 00:35:46 +0200

qmapshack (1.3.1-2) unstable; urgency=medium

  * Use desktop file in favor of menu file per CTTE #741573.
  * Add patch to install QMapShack icon in hicolor theme too.
  * Rebuild for gdal transition.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 22 Oct 2015 20:41:53 +0200

qmapshack (1.3.1-1) unstable; urgency=medium

  * New upstream release.
  * Drop license & copyright for 3rdparty code, no longer included.
  * Add build dependency on libroutino-dev.
  * Override dh_install to use --list-missing.
  * Don't remove planetsplitter, no longer built.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 18 Sep 2015 15:43:39 +0200

qmapshack (1.3.0-4) unstable; urgency=medium

  * Disable icon regeneration on arm64 due to inkscape failure.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 26 Aug 2015 02:09:49 +0200

qmapshack (1.3.0-3) unstable; urgency=medium

  * Update Vcs-Browser URL to use HTTPS.
  * Update watch file to support more version formats.
  * Rebuild for gdal transition.

 -- Bas Couwenberg <sebastic@debian.org>  Tue, 25 Aug 2015 17:59:55 +0200

qmapshack (1.3.0-2) unstable; urgency=medium

  * Add dependency on routino for planetsplitter.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 01 Jul 2015 19:19:39 +0200

qmapshack (1.3.0-1) unstable; urgency=medium

  * New upstream release.
  * Update copyright file, changes:
    - Update copyright years
    - Add license & copyright for new 3rdparty routino source
  * Don't install planetsplitter, binary already provided by routino package.

 -- Bas Couwenberg <sebastic@debian.org>  Tue, 30 Jun 2015 20:31:51 +0200

qmapshack (1.2.2-1) unstable; urgency=medium

  * New upstream release.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 30 Apr 2015 21:56:47 +0200

qmapshack (1.2.1-1) unstable; urgency=medium

  * Move from experimental to unstable.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 26 Apr 2015 19:56:22 +0200

qmapshack (1.2.1-1~exp1) experimental; urgency=medium

  * New upstream release.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 25 Apr 2015 16:15:56 +0200

qmapshack (1.2.0-1~exp2) experimental; urgency=medium

  * Add dependency on libqt5sql5-sqlite to support database import from
    QLandkarteGT.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 17 Apr 2015 16:47:33 +0200

qmapshack (1.2.0-1~exp1) experimental; urgency=medium

  * New upstream release.

 -- Bas Couwenberg <sebastic@debian.org>  Tue, 14 Apr 2015 23:30:49 +0200

qmapshack (1.1.0-1~exp1) experimental; urgency=medium

  * New upstream release.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 29 Mar 2015 17:56:05 +0200

qmapshack (1.0.1-1~exp1) experimental; urgency=medium

  * New upstream release.
  * Drop upstream tarball repacking, no longer required.
  * Drop devicewatcher-hurd-kfreebsd.patch, applied upstream.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 19 Mar 2015 21:17:01 +0100

qmapshack (1.0.0+ds1-1~exp2) experimental; urgency=medium

  * Add patch to disable deviceWatcher on Hurd & kFreeBSD to fix FTBFS.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 08 Mar 2015 16:57:23 +0100

qmapshack (1.0.0+ds1-1~exp1) experimental; urgency=medium

  * New upstream release.
  * Enable verbose makefiles.
  * Remove unused variables in rules file.
  * Add upstream metadata.
  * Repack upstream tarball to exclude .hg directory.
  * Update my email to @debian.org address.
  * Remove patches applied upstream.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 07 Mar 2015 14:31:31 +0100

qmapshack (0.12.0-1~exp1) experimental; urgency=medium

  * New upstream release.

 -- Bas Couwenberg <sebastic@xs4all.nl>  Wed, 28 Jan 2015 08:47:23 +0100

qmapshack (0.11.0-1~exp1) experimental; urgency=medium

  * New upstream release.
  * Drop artificial-typo.patch, applied upstream.

 -- Bas Couwenberg <sebastic@xs4all.nl>  Mon, 19 Jan 2015 21:37:39 +0100

qmapshack (0.10.0-1~exp1) experimental; urgency=medium

  * New upstream release.
  * Add path to fix 'artificial' typo.

 -- Bas Couwenberg <sebastic@xs4all.nl>  Sun, 11 Jan 2015 12:08:03 +0100

qmapshack (0.9.1-1~exp1) experimental; urgency=medium

  * New upstream release.
  * Refresh patches.
  * Reorder targets in rules file, drop get-orig-source target plain
    uscan is used now.
  * Reorder Files paragraphs in copyright file, place debian/* at the end.

 -- Bas Couwenberg <sebastic@xs4all.nl>  Fri, 02 Jan 2015 13:27:08 +0100

qmapshack (0.9.0-1~exp1) experimental; urgency=medium

  * New upstream release.
  * Update Source URL in copyright file.
  * Restructure control file with cme.
  * Use cgit instead of gitweb for Vcs-Browser URL.

 -- Bas Couwenberg <sebastic@xs4all.nl>  Thu, 25 Dec 2014 17:22:23 +0100

qmapshack (0.8.2-1~exp1) experimental; urgency=medium

  * New upstream release.
  * Add myself to Uploaders.
  * Update copyright file, changes:
    - Copyright years
    - public-domain license specification
  * Remove icons before regenerating in d/rules instead of d/clean.

 -- Bas Couwenberg <sebastic@xs4all.nl>  Sat, 13 Dec 2014 02:03:10 +0100

qmapshack (0.7.0-1) unstable; urgency=medium

  * Imported Upstream version 0.7.0
  * Add qtscript5-dev as build dep.
  * Build with dh parallel.
  * Patch refreshed.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Wed, 29 Oct 2014 18:34:52 +0100

qmapshack (0.6.0-1) unstable; urgency=medium

  * Start new upload.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Thu, 16 Oct 2014 21:41:48 +0200

qmapshack (0.5.0-1) unstable; urgency=low

  * Imported Upstream version 0.5.0
  * Remove qt5-default from build deps.
  * Bump Standards.
  * Update copyright file.
  * Provide code for Exec key in desktop file.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Tue, 07 Oct 2014 12:47:09 +0200

qmapshack (0.3.0-1) unstable; urgency=low

  * Initial release (Closes: #756064).

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Sun, 17 Aug 2014 18:07:05 +0200
